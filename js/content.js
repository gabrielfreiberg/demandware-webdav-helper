/**************************************************************
 * document.ready()
 **************************************************************/
$(function() {

    dwLogHelper.app.init();

});

/**************************************************************
 * dwLogHelper
 **************************************************************/
(function(dwLogHelper, $) {

    var $cache,
        href = window.location.href,
        isFile = false,
        monitorId;

    function initCache() {
        $cache = {
            table: $('table').length === 1 ? $('table') : {}
        }
    }

    function buildTablesorter() {
        if (!isFile) {
            // first row #first-row, table .tablesorter
            $cache.table.find('tr:first-child').attr('id', 'first-row');
            $cache.table.addClass('tablesorter');

            // Replace first row td with th
            $("#first-row td[align='left'] font").unwrap().wrap("<th align='left'></th>");
            $("#first-row td[align='center'] font").unwrap().wrap("<th align='center'></th>");
            $("#first-row td[align='right'] font").unwrap().wrap("<th align='right'></th>");

            // Add thead and move first row
            $("table tbody").before("<thead></thead>");
            $("#first-row").appendTo("table thead");

            // Move parent directory link to thead
            if ($("table tbody tr:first-child a").text().indexOf("Parent Directory") !== -1) {
                $("table tbody tr:first-child").appendTo("table thead").find("a").addClass("parent-directory");
            }

            // Add images
            $cache.table.find("tbody tr td:first-child a").each(function(ind, elem) {
                var splitDot = $(elem).text().split("."),
                    fileExtension = splitDot.length === 1 ? "folder" : splitDot[splitDot.length - 1];

                $(elem).html($(elem).html().replace(/&nbsp;/g, ''));
                $(elem).addClass(fileExtension);
            });

            $.tablesorter.addParser({
                // set a unique id 
                id: 'dwdate',
                is: function(s) {
                    // return false so this parser is not auto detected
                    return false;
                },
                format: function(s) {
                    // format your data for normalization 
                    return Date.parse(s);
                },
                // set type, either numeric or text 
                type: 'numeric'
            });

            // Init tablesorter
            $cache.table.tablesorter({
                headers: {
                    2: {
                        sorter: 'dwdate'
                    }
                }
            });
        }
    }

    function buildBreadcrumbs() {
        var findRootStr = "/on/demandware.servlet/webdav/Sites/",
            findRootInd = href.indexOf(findRootStr),
            webDavRootStr = href.slice(0, findRootInd + findRootStr.length),
            relPathArr = href.slice(findRootInd + findRootStr.length, href.length).split("/"),
            relRootStr = relPathArr[0],
            rootsArr = ["Cartridges", "Impex", "Logs", "Realmdata", "Securitylogs", "Static", "Temp"],
            rootSelect = '<ul>',
            links = '<ul id="root-droplist">';

        // Build root elements
        for (var i = 0; i < rootsArr.length; i++) {
            if (rootsArr[i] === relRootStr) continue;
            rootSelect += '<li><a href="' + webDavRootStr + rootsArr[i] + '/">' + rootsArr[i] + '</a></li>';
        }
        rootSelect += '</ul>';

        // Build horizontal elements
        for (var i = 0; i < relPathArr.length; i++) {
            
            if (relPathArr[i] == "" || relPathArr[i] == " ") continue;
            
            var rel = "";
            
            for (var j = 0; j < i; j++) {
                rel += relPathArr[j] + "/";
            }

            if (relPathArr[i].indexOf(".") !== -1) {
                isFile = true;
            }

            if (i === 0) {
                links += '<li>/<a class="root" href="' + webDavRootStr + rel + relPathArr[i] + "/" + '">' + relPathArr[i] + '</a>/' + rootSelect + '</li>';
            } else {
                links += '<li><a href="' + webDavRootStr + rel + relPathArr[i] + "/" + '">' + relPathArr[i] + '</a>' + (isFile ? '' : '/') + '<li>';
            }
        }

        // Add menu icons and close ul
        if(isFile){
            links += '<li class="menu-icon watch"><span id="ajax-status"></span>Watch</li>';
        }
        links += "</ul>";

        // Append ul
        if ($('body > h1').length > 0) {
            $('body > h1').addClass('main-nav').html(links);
        } else {
            $('<h1 class="main-nav">' + links + '</h1>').appendTo('body');
        }

        // Adjust top padding for content height + 40
        $('body').css({
            'padding-top': $('body > h1').height() + 40
        });

        // Call vanilla plugin
        $('#root-droplist').jVanilla({
            speed: 50,
            animation: 'sliding',
            eventType: 'hover',
            delay: 100,
            isHoverClickable: true,
            isLastRightAlign: false
        });


        // Bind monitor events to link
        var toggle = false;
        $("#root-droplist .watch").on("click", function(){
            console.log("click");
            toggle = !toggle;
            // Bind monitor events to link
            if(toggle) {
                monitor();
                $(this).addClass("active");    
            } else {
                killMonitor();
                $(this).removeClass("active");      
            }
        });
    }

    function monitor() {
        console.log("monitor");
        if (isFile) {
            console.log("isfile");
            var modifiedCount = 0,
                dmp = new diff_match_patch();

            monitorId = setInterval(function() {
                //console.log('Ping... window.location.href=' + window.location.href);
                $("#ajax-status").removeClass("green").removeClass("red");
                $.ajax(window.location.href, {
                    ifModified: true,
                    success: function(data, textStatus, jqXHR) {
                        if (textStatus == 'success') {
                            $("#ajax-status").addClass("green");
                            modifiedCount++;
                            console.log('textStatus:' + textStatus);
                            console.log('modifiedCount:' + modifiedCount);
                            if (modifiedCount > 1) {
                                //console.log('data:' + data);
                                var diffs = dmp.diff_main($($('pre')[0]).text(), data);
                                $('pre').append('<span class="diff' + (modifiedCount % 2 == 0 ? ' even' : ' odd') + '">' + diffs[diffs.length - 1] + '</span>');
                                $('span.diff').on('click', function(e) {
                                    $(this).off();
                                    console.log('clicked me');
                                    $(this).removeClass('even').removeClass('odd');
                                });
                            }
                        }
                        else{
                            $("#ajax-status").addClass("red");
                        }
                    }
                });
            }, 2000);
        }
    }

    function killMonitor() {
        if(monitorId !== null){
            clearInterval(monitorId);
        }
    }

    dwLogHelper.app = {
        init: function() {
            initCache();
            buildBreadcrumbs();
            buildTablesorter();
            //monitor();
        }
    }
})(window.dwLogHelper = window.dwLogHelper || {}, jQuery);