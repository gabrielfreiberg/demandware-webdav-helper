# README #

Demandware WebDav Helper adds some basic functionality to the WebDav file listing pages. Not a replacement for a proper text editor or log viewer, but helpful for lazy people who need to access WebDav directories quickly on a bunch of different environments.

### How do I get set up? ###
1. Clone the repository
2. Open chrome, navigate to chrome://extensions
3. Check "Developer Mode" in the top right corner
4. Click "Load Unpacked Extension", navigate to the demandware-webdav-helper folder, click "Open"
5. Go browse your WebDav directory in Business Manager via Administration -> Site Development -> Development Setup

### Features ###

* Table sorting for file listings. Double-sort by hold shift.
* File extension icons.
* Top-level directories menu (IMPEX, TMP, CARTRIDGES, etc).
* File-path breadcrumbs.
* Watch a log file.

### Contributing ###
I wrote this a couple of years ago and never made the repo public until now.  It needs a lot of cleanup, but it generally works pretty well.  Happy to review and accept pull requests.